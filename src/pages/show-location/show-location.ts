import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;

@IonicPage()
@Component({
  selector: 'page-show-location',
  templateUrl: 'show-location.html',
})
export class ShowLocationPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  user_data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private geo: Geolocation) {
    this.user_data = JSON.parse(localStorage.getItem('User Data'))
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShowLocationPage');
    this.loadMap();
  }

  goBack() {
    this.navCtrl.pop()
  }

  loadMap() {
    this.geo.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      let self = this;
      setTimeout(function () {
        self.addMarker()
      }, 500)
    }, (err) => {let latLng = new google.maps.LatLng('28.5007198', '77.0697248');
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      let self = this;
      setTimeout(function () {
        self.addMarker()
      }, 500)
    });

  }

  addMarker(){
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter()
    });
    let content = "<p>Hello User ! You are currently here.. </p>";         
    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content){
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }

}
