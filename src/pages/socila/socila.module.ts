import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SocilaPage } from './socila';

@NgModule({
  declarations: [
    SocilaPage,
  ],
  imports: [
    IonicPageModule.forChild(SocilaPage),
  ],
})
export class SocilaPageModule {}
