import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Instagram } from '@ionic-native/instagram';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
/**
 * Generated class for the SocilaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-socila',
  templateUrl: 'socila.html',
})
export class SocilaPage {
  database: any =null;
  items: Array<Object>;

  constructor(private instagram: Instagram,public navCtrl: NavController, public navParams: NavParams,private socialSharing: SocialSharing,public sq : SQLite) {
    this.database = new SQLite();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SocilaPage');
  }

email(){
  this.socialSharing.canShareViaEmail().then(() => {
    // Sharing via email is possible
  }).catch(() => {
    // Sharing via email is not possible
  });
  
  // Share via email
  this.socialSharing.shareViaEmail('Body', 'Subject', ['recipient@example.org']).then(() => {
    // Success!
  }).catch((err) => {
    console.log(err)
  });
}

fb(){

  // Share via email
  this.socialSharing.shareViaFacebook('Body', 'https://www.facebook.com').then(() => {
    // Success!
  }).catch((err) => {
    console.log(err)
  });
}
twit(){
  // Share via email
  this.socialSharing.shareViaTwitter('Body', 'https://www.twitter.com').then(() => {
    // Success!
  }).catch((err) => {
    // Error!
    console.log(err)
  });
}
msg(){
  
  // Share via email
  this.socialSharing.shareViaSMS('Body', '9536566195').then(() => {
    // Success!
  }).catch((err) => {
    console.log(err)
  });
}
whatsapp(){

  // Share via email
  this.socialSharing.shareViaWhatsApp('Body', 'https://www.whatsapp.com').then(() => {
    // Success!
  }).catch((err) => {
    console.log(err)
  });
}

insta(){
  this.instagram.share('data:image/png;uhduhf3hfif33', 'Caption')
  .then(() => console.log('Shared!'))
  .catch((error: any) => console.error(error));
}

db(){
  this.database.create({name: "data.db", location: "default"}).then((db: SQLiteObject) => {
    db.executeSql("INSERT INTO people (firstname, lastname) VALUES ('Nic', 'Raboy')", []).then((data) => {
      console.log("INSERTED: " + JSON.stringify(data));
  }, (error) => {
      console.log("ERROR: " + JSON.stringify(error.err));
  });
    
  }, (error) => {
      console.log("ERROR: ", error);
  });

}

dbshow(){
  this.database.create({name: "data.db", location: "default"}).then((db: SQLiteObject) => {
    db.executeSql("SELECT * FROM people", []).then((data) => {
      this.items = [];
      if(data.rows.length > 0) {
          for(var i = 0; i < data.rows.length; i++) {
              this.items.push(data.rows.item(i));
          }
      }
      console.log(JSON.stringify(this.items));
  }, (e) => {
  
      console.log("Errot: " + JSON.stringify(e));
  });
    
  }, (error) => {
      console.log("ERROR: ", error);
  });

}
 

}

