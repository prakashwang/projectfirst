import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, LoadingController, AlertController, Events } from 'ionic-angular';
import { App } from 'ionic-angular/components/app/app';
import { CameraproviderProvider } from '../../providers/cameraprovider/cameraprovider';

@IonicPage()
@Component({
    selector: 'page-info',
    templateUrl: 'info.html',
})
export class InfoPage {
    imageObject: any = {};
    constructor(public navCtrl: NavController, public navParams: NavParams, private app: App, public action: ActionSheetController, public events: Events, private camera_provider: CameraproviderProvider) { }
    // esignData: any;
    ionViewDidLoad() {
        console.log('ionViewDidLoad DocumentUploadPage');
    }
    /* Navigate back to previous page */
    goBack() {
        this.navCtrl.pop()
    }

    /* Upload identification Image */
    uploadPic() {
        let actionSheet = this.action.create({
            title: 'Upload Your Picture',
            buttons: [
                {
                    text: 'Camera',
                    handler: () => {
                        // actionSheet.dismiss();
                        // this.imageObject = this.camera_provider.imageClickFunction('Camera')
                        // console.log(this.imageObject)
                        this.camera_provider.imageClickFunction('Camera').then(result => {
                            this.imageObject = result
                        }).catch(err => {
                            console.log('Error occured')
                        })
                    }
                },
                {
                    text: 'Gallery',
                    handler: () => {
                        // actionSheet.dismiss();
                        // this.imageObject = this.camera_provider.imageClickFunction('Gallery')
                        // console.log(this.imageObject)
                        this.camera_provider.imageClickFunction('Gallery').then(result => {
                            this.imageObject = result
                        }).catch(err => {
                            console.log('Error occured')
                        })
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    }
}