import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Stripe } from '@ionic-native/stripe';

/**
 * Generated class for the StripePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-stripe',
  templateUrl: 'stripe.html',
})
export class StripePage {
  striped: object = {};
  card: any;
  token: any;
  type: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private stripe: Stripe) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StripePage');
  }
  getToken() {
    this.stripe.setPublishableKey('pk_test_oi0sKPJYLGjdvOXOM8tE8cMa');
    console.log(this.striped)
    console.log(this.striped['cardNumber'])
    console.log(this.striped['expiryMonth'])
    console.log(this.striped['expiryYear'])
    console.log(this.striped['cvc'])
    this.card = {
      number: this.striped['cardNumber'],
      expMonth: this.striped['expiryMonth'],
      expYear: this.striped['expiryYear'],
      cvc: this.striped['cvc']
    };

    this.stripe.createCardToken(this.card)
      .then(token => {
        console.log(token.id)
        this.token = token.id
        this.stripe.getCardType(this.card.number)
          .then(card => {
            this.type = card;

          })
          .catch(error => console.error(error));
      })
      .catch(error => console.error(error));

  }


  /*
  
  app.controller('atsPositionImportCtrl', [
    '$scope',
    '$rootScope',
    '$state',
    '$timeout',
    'jobPostingFactory',
    'utilityFactory',
    'mdCommonModal',
    'toasterService',
    function ($scope, $rootScope, $state, $timeout, jobPostingFactory, utilityFactory, mdCommonModal, toasterService) {
        function init() {
          $scope.isProcessing = false;
          $scope.uploadSource = 'MYCOMPUTER';
          $scope.count = 0;
          utilityFactory.deleteFromFactoryStore('position');
          $scope.view = 'IMPORT';
          $scope.progress = 0;
      }

      $scope.uploadPosition = function (files, errFiles) {
          $scope.numberOfFiles = files.length;
          $scope.srcClass = 'file';
          $scope.files = files;
          $scope.errFiles = errFiles;
          if (errFiles && errFiles.length) {
              return;
          }
          $scope.isProcessing = $scope.numberOfFiles ? true : false;
          sendFile(files);
      }

      function sendFile(files) {
          var fileUploadedCount = 0
          var maxSize = 5 * 1024 * 1024;
          var filesArray = [];
          $scope.progress = 0;
          $scope.totalFileSize = _.sumBy(files, function (f) {
              return f.size
          })
          angular.forEach(files, function (file, i) {

              if (file.size > 5 * 1024 * 1024) {
                  fileUploadedCount++;
                  upload(files.length, file, fileUploadedCount);
              } else if (file.size > maxSize) {
                  fileUploadedCount++;
                  upload(files.length, filesArray, fileUploadedCount);
                  maxSize = 5 * 1024 * 1024;
                  filesArray = [];
                  filesArray.push(file);
              } else {
                  fileUploadedCount++;
                  maxSize = maxSize - file.size;
                  filesArray.push(file);
              }
          });
          if (filesArray.length) {
              upload(files.length, filesArray, fileUploadedCount);
          }
      }

      function upload(length, files, completed) {
          $scope.progress = 0;
          var queryParams = 'clientId=' + utilityFactory.getItemInLocalStorage('CLIENT_ID');
          jobPostingFactory.uploadPosition(files, queryParams)
              .then(function (response) {
                  // (length == completed) && handleUploadCallBack(response);
                  $timeout(function () {
                      $scope.result = response.data;
                      if (response.data.error != 'custom error 1') {
                          (length == completed) && handleUploadCallBack(response, 'Successfully created Position(s)');
                          utilityFactory.setFactoryStore('job', length);
                      } else {
                          // alert('Error')
                          handleUploadCallBack(response, 'Something went wrong. Please try again after some time')
                      }
                  });
              }, function (response) {
                  if (response.status > 0) {
                      $scope.errorMsg = response.status + ': ' + response.data;
                  }
              }, function (evt) {
                  getUploadProgress(evt)
              });
      }

      function getUploadProgress(evt) {
          var percent = Math.min(100, parseInt(100.0 * evt.loaded / $scope.totalFileSize))
          $scope.progress = $scope.progress < percent ? percent : $scope.progress;
      }

      // function startPooling(sessionId, isRetry, msg) {
      //     console.log('timer');
      //     msg = msg ? msg : 'Resumes uploaded successfully'
      //     timer = $timeout(function () {
      //         getUploadResult(sessionId, isRetry, msg)
      //     }, 1000);
      // }

      // will try to make it independent function.
      // function getUploadResult(sessionId, isRetry, msg) {
      //     jobPostingFactory.getUploadResult({
      //         sessionId: sessionId,
      //         count: $scope.numberOfFiles
      //     })
      //         .then(function (response) {

      //             if (response.data && response.data.data) {
      //                 var data = response.data.data;
      //                 var filesProcessed = data.filesProcessed || 0;
      //                 var ended = data.retry.ended || 0;

      //                 if (!isRetry && (filesProcessed < data.totalFiles)) {
      //                     startPooling(sessionId)
      //                 } else if (isRetry && (data.retry.total > ended)) {
      //                     startPooling(sessionId, isRetry, msg)
      //                 } else {

      //                 }
      //             } else {
      //                 $scope.isProcessing = false;
      //                 var mssg = isRetry ? 'Successfully updated Resumes' : 'Successfully uploaded Resumes'
      //                 toasterService.show(mssg);
      //             }
      //         }, function (error) {
      //             console.log('error', error);
      //             toasterService.show('Some error occured in upload, please try after sometime');
      //             $scope.isProcessing = false;
      //         }).catch(function (err) {
      //             console.log('err', err);
      //             toasterService.show('Some error occured in upload, please try after sometime');
      //         })

      //     clearTimeout(timer);
      // }

      function handleUploadCallBack(data, msg) {
          $timeout(function (data) {
              $scope.progress = 100;
              $scope.isProcessing = false;
          }, 50)
          // utilityFactory.setFactoryStore('resume', data.filesProcessed)
          // $scope.resumeUploaded.total = data.totalFiles;
          toasterService.show(msg);
          $scope.goBack();
      }

      $scope.showCancelUploadingDialog = function (e) {
          mdCommonModal.show(e, {
              message: "All your progress will be lost if you cancel the uploading process",
              action: $scope.cancelUploading
          })
      };

      $scope.cancelUploading = function () {
          // $scope.isProcessing = false;
          mdCommonModal.hide();
      }

      $scope.goBack = function () {
          if ($rootScope._previousState) {
              $rootScope._goBack();
          } else {
              $state.go('main.import');
          }
      }

      init();

  }
]);
  
  */
}
