import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { InfoPage } from '../info/info';
import { ShowLocationPage } from '../show-location/show-location';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  user_info: any = {}
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    this.user_info = JSON.parse(localStorage.getItem('User Data'))
  }

  ionViewWillEnter() {
  }

  goToImageCaptirePage() {
    this.navCtrl.push(InfoPage)
  }

  userLocation() {
    this.navCtrl.push(ShowLocationPage)
  }

}
