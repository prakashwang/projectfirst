import { Injectable } from '@angular/core';
// import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HttpClient } from '@angular/common/http';

/*
  Generated class for the CameraproviderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CameraproviderProvider {

  constructor(public http: HttpClient, private camera: Camera) {
    console.log('Hello CameraproviderProvider Provider');
  }

  imageClickFunction(type) {
    return new Promise((resolve, reject)=>{
      if (type == 'Camera') {
        const options: CameraOptions = {
          quality: 40,
          destinationType: this.camera.DestinationType.DATA_URL,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE
        }
        this.camera.getPicture(options).then((imageData) => {
          let image = 'data:image/jpeg;base64,' + imageData;
          resolve({ 'base64': imageData, 'imageToShow': image })
        }, (err) => {
          reject(err)
        });
      } else {
        const options: CameraOptions = {
          quality: 40,
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE
        }
        this.camera.getPicture(options).then((imageData) => {
          let image = 'data:image/jpeg;base64,' + imageData;
          resolve({ 'base64': imageData, 'imageToShow': image })
        }, (err) => {
          reject(err)
        });
      }
    })
  }

}
